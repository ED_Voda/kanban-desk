import {EventDispather} from "./EventDispather";


import {Task} from "./task";


/**
 * @see {EventDispather}
 */
class Kanban extends EventDispather{
    /**
     * Constructor of Kanban
     * @param options {Object} Object of the options used from
     */
    constructor(options){
        super();
        this.tasks = [];

        this.node = document.body.querySelector(options.selector);
        this.conteiners = this.node.querySelectorAll('.tasks');
        this.conteinerTypes = {};

        for(let node of this.conteiners){
            this.conteinerTypes[node.dataset.type] = node.dataset.name;
        }

        this.lastId = -1;
    }

    /**
     * Download from JSON
     * @param json
     */
    fromJSON(json){

        let data = JSON.parse(json);

        this.tasks.splice();
        this.lastId = data.lastId;
        for (let task of data.tasks){
            let item = this.createTask(task.title);
            item.id = task.id;
            item.status= task.status;
        }

        this.dispatch('load', this);
    }

    /**
     * Save to JSON file
     */
    save(){
        let a = document.createElement("a");
        let file = new Blob([JSON.stringify(this.toJSON())], {type:'text/plain'});
        a.href = URL.createObjectURL(file);
        a.download = "kanban";
        a.click();
    }

    /**
     * Write to JSON file
     * @returns {{last: (number|*), tasks: Array}}
     */
    toJSON(){
        let info = {
            last: this.lastId,
            tasks: []
        };

        this.tasks.forEach((task)=>{
            info.tasks.push(
                task.toJSON()
            )
        })


        return info;
    }

    /**
     * This function get types of container
     * @param type
     * @returns {*}
     */
    getContainerByType(type){
            for(let cont of this.conteiners){
                if(cont.dataset.type && cont.dataset.type === type){
                    return cont;
                }
            }

            return null;
    }

    /**
     * This function create task
     * @param title
     * @returns {Task}
     */
    createTask(title){
        let task = new Task({
            title,
            id : ++this.lastId
        });

        task.on('statuschange',(item)=>{
            console.log(item._status);
            let container = this.getContainerByType(item._status);
            if (container) {
                container.appendChild(item._node);
                this.dispatch("taskstatuschange",item);

            }
        });


        this.tasks.push(task);
        task.render(this.conteinerTypes);
        this.conteiners[0].appendChild(task._node);

        return task;
    }
}

window.Kanban = Kanban;