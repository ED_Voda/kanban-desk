/**
 * Class for capturing and changing data at events
 */
export class EventDispather{
    /**
     * EventDispather constructor
     */
    constructor(){
        this._events = {};

    }

    /**
     * Function on
     * @param event - Accepts an event and checks its availability.
     * @param callback - Event returns
     */
    on(event, callback){
        if (!this._events[event]){
            this._events[event] = [];
        }
            this._events[event].push(callback);
    }

    /**
     * Changes data at event
     * @param event - Accepts an event and checks its availability
     * @param data - Changes data at event
     * @returns {boolean}
     */
    dispatch(event, data){
        if(!this._events[event])
        {
            return false;
        }
        for(let target of this._events[event]){
            target(data);
        }
    }
}