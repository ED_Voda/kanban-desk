
import {EventDispather} from "./EventDispather";
/**
 * @see {EventDispather}
 */
export class Task extends EventDispather{
    /**
     * Construcotr of the Task class
     * @param title - Task name
     * @param id - Task number
     */
    constructor({title='', id= null}){
        super();
        this.title = title;
        this.id= id ;
        this._status = null;
        this._node = null;
    }

    /**
     * Serves to obtain a status
     * @returns {null}
     */
    get status(){
        return this._status;
    }

    /**
     * Serves to change the status
     * @param value
     */
    set status(value){
        this._status = value;
        this.dispatch('statuschange',this);
    }

    /**
     * Funtion for HTML code
     * @returns {string}
     */
    static getTemplate(){
        return `<p class="title">
                        $TITLE
                    </p>
                    <label class="label">Status</label>
                    <select></select>`
    }

    /**
     * The function of converting to a class date JSON
     * @returns {{title: string, id: *, status: null}}
     */
    toJSON(){
        return {
            title: this.title,
            id: this.id,
            status: this.status
        }
    }

    /**
     * Function render Task on page
     * @param types
     * @returns {HTMLElement}
     */
    render(types){
        let div = document.createElement("div");
        div.classList.add('task');

        let template = Task.getTemplate();
        template = template.replace('$TITLE',this.title);

        div.innerHTML = template;

        let select = div.querySelector("select");
        console.dir(select);
        for (let key in types){
            let opt = document.createElement('option');
            opt.text = types[key];
            opt.value = key;
            select.add(opt);
        }

        select.addEventListener('change', ()=>{
            this.status = select.value;
        });

        this._node = div;
        return div;
    }
}