# Kanban Desk

kanban Desk  is a tool with which you can create a task Board with your own design.
You can use such functionality as:
- Creating your tasks
- Change task status
- Import and export task
### Tech
Kanban Desk uses a number of open source projects to work properly:
* [node.js] - evented I/O for the backend
* [webpack] - the streaming build system
## Installation

Install the dependencies and devDependencies and start the local server.

```sh
$ cd Kanban-desk
$ npm install 
```

## Development

Kanban Desk uses Webpack for fast developing.
Make a change in your file and instantanously see your updates.

Open your favorite Terminal and run these commands.

First Tab:
```sh
$ npm start
```

#### Optional

You can use lind to make your code uniform

```sh
$ npm lind
```

#### Building for source
For production release:
```sh
$ npm build
```

### HTML script example
Create an instance of the class and designate it through the selector. This is your task table.
```sh
    let kanban = new Kanban({
        selector: '#desk'
    });
```
Form for adding tasks
```sh
    let form = document.getElementById('newTask')
    
    /*
    Button to confirm adding.
    */
    form.addEventListener('submit',(e)=>{
        e.preventDefault();
        let taskname = form.elements.taskname.value;
        
        if(taskname){
            kanban.createTask(taskname);
        }
        form.reset();
    });
```
Form to load the data
```sh
   let formLoad = document.getElementById('loadTask')
   
        /*
            Button to confirm loading.
        */
       formLoad.addEventListener('submit',(e)=>{
           e.preventDefault();
   
           let content = formLoad.elements.json.value;
   
           if(content){
               kanban.fromJSON(content);
           }
   
           formLoad.reset();
   
       });
```





[node.js]: <http://nodejs.org>
[webpack]: <https://webpack.js.org/>